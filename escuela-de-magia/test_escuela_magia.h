#ifndef TEST_ESCUELA_MAGIA_H
#define TEST_ESCUELA_MAGIA_H

#include <iostream>
#include "escuela_magia.h"
#include "../common/Common.h"
#include "../common/Test.h"

using namespace std;

///***************************** Tests de Hechizo *****************************/

TEST(test_estaVacia_falso, {
    EscuelaDeMagia e = fundarEscuela();
    registrar("Harry", e);
	ASSERT_EQ(estaVacia(e), false);
})

TEST(test_estaVacia_verdadero, {
    EscuelaDeMagia e = fundarEscuela();
	ASSERT_EQ(estaVacia(e), true);
})

TEST(test_faltanAprender_dos, {
    EscuelaDeMagia e = fundarEscuela();
    registrar("Harry", e);
    registrar("pepe", e);
    Hechizo h1 = crearHechizo("fuego", 99);
    Hechizo h2 = crearHechizo("Hielazo", 99);
    Hechizo h3 = crearHechizo("LLamarada", 99);
    enseniar(h1, "Harry", e);
    enseniar(h2, "Harry", e);
    enseniar(h3, "Harry", e);
    enseniar(h2, "pepe", e);
	ASSERT_EQ(leFaltanAprender("pepe", e), 2);
})

TEST(test_faltanAprender_ninguno_pepe, {
    EscuelaDeMagia e = fundarEscuela();
    registrar("Harry", e);
    registrar("pepe", e);
    Hechizo h1 = crearHechizo("fuego", 99);
    Hechizo h2 = crearHechizo("Hielazo", 99);
    Hechizo h3 = crearHechizo("LLamarada", 99);
    enseniar(h1, "pepe", e);
    enseniar(h2, "pepe", e);
    enseniar(h3, "pepe", e);
	ASSERT_EQ(leFaltanAprender("pepe", e), 0);
})

TEST(test_unEgresado, {
    EscuelaDeMagia e = fundarEscuela();
    registrar("Harry", e);
    registrar("pepe", e);
    registrar("El mago Emanuel", e);
    Hechizo h1 = crearHechizo("fuego", 99);
    Hechizo h2 = crearHechizo("Hielazo", 99);
    Hechizo h3 = crearHechizo("LLamarada", 99);
    enseniar(h1, "Harry", e);
    enseniar(h2, "Harry", e);
    enseniar(h1, "pepe", e);
    enseniar(h2, "pepe", e);
    enseniar(h3, "pepe", e);
	ASSERT_EQ(nombreMago(unEgresado(e)),"pepe");
})

TEST(test_quitar_todos_los_egresados, {
    EscuelaDeMagia e = fundarEscuela();
    registrar("Harry", e);
    registrar("pepe", e);
    registrar("El mago Emanuel", e);
    Hechizo h1 = crearHechizo("fuego", 99);
    Hechizo h2 = crearHechizo("Hielazo", 99);
    Hechizo h3 = crearHechizo("LLamarada", 99);
    enseniar(h1, "Harry", e);
    enseniar(h2, "Harry", e);
    enseniar(h1, "pepe", e);
    enseniar(h2, "pepe", e);
    enseniar(h3, "pepe", e);
    quitarEgresado(e);
    quitarEgresado(e);
    quitarEgresado(e);
	ASSERT_EQ(estaVacia(e),true);
})

TEST(test_quitar_dos_egresados, {
    EscuelaDeMagia e = fundarEscuela();
    registrar("Harry", e);
    registrar("pepe", e);
    registrar("El mago Emanuel", e);
    Hechizo h1 = crearHechizo("fuego", 99);
    Hechizo h2 = crearHechizo("Hielazo", 99);
    Hechizo h3 = crearHechizo("LLamarada", 99);
    enseniar(h1, "Harry", e);
    enseniar(h2, "Harry", e);
    enseniar(h1, "pepe", e);
    enseniar(h2, "pepe", e);
    enseniar(h3, "pepe", e);
    quitarEgresado(e);
    quitarEgresado(e);
	ASSERT_EQ(estaVacia(e),false);
})


void correrTestEscuela() {
   test_estaVacia_falso();
   test_estaVacia_verdadero();
   test_faltanAprender_dos();
   test_faltanAprender_ninguno_pepe();
   test_unEgresado();
   test_quitar_todos_los_egresados();
   test_quitar_dos_egresados();
}

#endif


