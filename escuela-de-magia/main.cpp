#include <iostream>
#include <cstdlib>
#include <vector>
#include "../hechizo/hechizo.h"
#include "../mago/mago.h"
#include "../set/set.h"
#include "../map/map.h"
#include "../maxheap/maxheap.h"
#include "escuela_magia.h"
#include "../common/Common.h"
#include "test_escuela_magia.h"

using namespace std;

/// Proposito: Retorna todos los hechizos aprendidos por los magos.
/// Eficiencia: ?

Set hechizosAprendidos(EscuelaDeMagia m) {
    Set hechizosEscuela = emptyS();
    for(int i = 0; i < magos(m).size(); i++){
        hechizosEscuela = unionS(hechizosEscuela,hechizosDe(magos(m)[i],m));
    }
    return hechizosEscuela;
}

/// Proposito: Indica si existe un mago que sabe todos los hechizos enseñados por la escuela.
/// Eficiencia: log (m) (costo heredado de leFaltanAprender)
bool hayUnExperto(EscuelaDeMagia m) {
   return leFaltanAprender(nombreMago(unEgresado(m)), m) == 0;
}

/// Proposito: Devuelve una maxheap con los magos que saben todos los hechizos dados por la escuela, quitándolos de la escuela.
/// Eficiencia: O ( n . (Log (m) + log (n) ) )
MaxHeap egresarExpertos(EscuelaDeMagia m){
    MaxHeap egresados = emptyH();
    while (hayUnExperto(m)){
        insertH(unEgresado(m),egresados);
        quitarEgresado(m);
    }
    return egresados;
}

void testearHechizosAprendidos(){
    cout << endl;
    cout << "Testeo de funcion Hechizos Aprendidos: " << endl;
    cout << endl;
    EscuelaDeMagia e = fundarEscuela();
    registrar("Harry", e);
    registrar("pepe", e);
    Hechizo h1 = crearHechizo("fuego", 99);
    Hechizo h2 = crearHechizo("Hielazo", 99);
    Hechizo h3 = crearHechizo("LLamarada", 99);
    enseniar(h1, "Harry", e);
    enseniar(h2, "Harry", e);
    enseniar(h3, "pepe", e);
    enseniar(h2, "pepe", e);
    if(belongsS(h1,hechizosAprendidos(e)) && belongsS(h2,hechizosAprendidos(e)) && belongsS(h3,hechizosAprendidos(e))){
            cout << "[ TEST   ] "  << endl;
            cout << "[     OK ] "  << endl;
    }
}

void testearhayUnExpertoVerdadero(){
    cout << endl;
    cout << "Testeo de hay un experto (Tiene que dar verdadero en este caso): " << endl;
    cout << endl;
    EscuelaDeMagia e = fundarEscuela();
    registrar("Harry", e);
    registrar("pepe", e);
    Hechizo h1 = crearHechizo("fuego", 99);
    enseniar(h1, "Harry", e);
    if(hayUnExperto(e)){
            cout << "[ TEST   ] "  << endl;
            cout << "[     OK ] "  << endl;
    }
}

void testearhayUnExpertoFalso(){
    cout << endl;
    cout << "Testeo de hay un experto (Tiene que dar falso en este caso): " << endl;
    cout << endl;
    EscuelaDeMagia e = fundarEscuela();
    registrar("Harry", e);
    registrar("pepe", e);
    Hechizo h1 = crearHechizo("fuego", 99);
    Hechizo h2 = crearHechizo("Hielazo", 99);
    enseniar(h1, "Harry", e);
    enseniar(h2, "pepe", e);
    if(!hayUnExperto(e)){
            cout << "[ TEST   ] "  << endl;
            cout << "[     OK ] "  << endl;
    }
}

void testearEgresarExpertos(){
    cout << endl;
    cout << "Testeo de Egresar Expertos: " << endl;
    cout << endl;
    EscuelaDeMagia e = fundarEscuela();
    registrar("Harry", e);
    registrar("pepe", e);
    Hechizo h1 = crearHechizo("fuego", 99);
    enseniar(h1, "Harry", e);
    egresarExpertos(e);
    if(hayUnExperto(e)){
            cout << "El test es incorrecto, no se egreso el experto" << endl;
    }
    else{
        cout << "[ TEST   ] "  << endl;
        cout << "[     OK ] "  << endl;
    }
}



int main()
{
    cout << "Testeo de funciones de interfaz: " << endl;
    cout << endl;
    testearHechizosAprendidos();
    testearhayUnExpertoFalso();
    testearhayUnExpertoVerdadero();
    testearEgresarExpertos();
    cout << endl;
    cout << "Testeo de funciones de implementacion escuela: " << endl;
    cout << endl;
    correrTestEscuela();
    return 0;
}
