#include "escuela_magia.h"
#include "../common/Common.h"

struct EscuelaDeMagiaSt {
   Set eHechizos;
   Map eMagosM;
   MaxHeap eMagosH;
};

/// Prop�sito: Devuelve una escuela vac�a.
/// O(1)
EscuelaDeMagia fundarEscuela() {
   EscuelaDeMagia e = new EscuelaDeMagiaSt;
   e -> eHechizos = emptyS();
   e -> eMagosM = emptyM();
   e -> eMagosH = emptyH();
   return e;
}

/// Prop�sito: Indica si la escuela est� vac�a.
/// O(1)
bool estaVacia(EscuelaDeMagia m) {
   return isEmptyH(m -> eMagosH);
}

/// Prop�sito: Incorpora un mago a la escuela (si ya existe no hace nada).
/// O(log m)
void registrar(string nombre, EscuelaDeMagia m) {
   if (lookupM(nombre, m -> eMagosM)==NULL){
        Mago mago = crearMago(nombre);
        assocM(nombre,mago, m -> eMagosM);
        insertH(mago,m -> eMagosH);
  }
}

/// Prop�sito: Devuelve los nombres de los magos registrados en la escuela.
/// O(m)
vector<string> magos(EscuelaDeMagia m) {
   return domM(m -> eMagosM);
}

/// Prop�sito: Devuelve los hechizos que conoce un mago dado.
/// Precondici�n: Existe un mago con dicho nombre.
/// O(log m)
Set hechizosDe(string nombre, EscuelaDeMagia m) {
   return hechizosMago(lookupM(nombre, m -> eMagosM));
}

/// Prop�sito: Dado un mago, indica la cantidad de hechizos que la escuela ha dado y �l no sabe.
/// Precondici�n: Existe un mago con dicho nombre.
/// O(log m)
int leFaltanAprender(string nombre, EscuelaDeMagia m) {
   return sizeS(m-> eHechizos) - sizeS(hechizosMago(lookupM(nombre, m -> eMagosM)));
}

/// Prop�sito: Devuelve el mago que m�s hechizos sabe.
/// Precondici�n: La escuela no est� vac�a.
/// O(log m)  [para mi es O(1)]
Mago unEgresado(EscuelaDeMagia m) {
   return maxH(m -> eMagosH);
}

/// Prop�sito: Devuelve la escuela sin el mago que m�s sabe.
/// Precondici�n: La escuela no est� vac�a.
/// O(log m)
void quitarEgresado(EscuelaDeMagia m) {
   deleteM(nombreMago(maxH(m -> eMagosH)),m -> eMagosM);
   deleteMax(m -> eMagosH);
}

/// Prop�sito: Ense�a un hechizo a un mago existente, y si el hechizo no existe en la escuela es incorporado a la misma.
/// O(m . log m + log h)
void enseniar(Hechizo h, string nombre, EscuelaDeMagia m) {
   addS(h, m -> eHechizos);
   Mago aux = lookupM(nombre, m -> eMagosM);
   MaxHeap nuevaH = emptyH();
   MaxHeap viejaH = m -> eMagosH;
   aprenderHechizo(h, aux);
   while (!isEmptyH(viejaH)){
            if(nombreMago(maxH(viejaH))!= nombre ){
                insertH(maxH(viejaH),nuevaH);
            }
            deleteMax(viejaH);
   }
   insertH(aux,nuevaH);
   m -> eMagosH = nuevaH;
   destroyH(viejaH);
}

/// Prop�sito: Libera toda la memoria creada por la escuela (incluye magos, pero no hechizos).
void destruirEscuela(EscuelaDeMagia m) {
    Mago aux = NULL;
    while (!isEmptyH(m -> eMagosH)){
        aux = maxH(m -> eMagosH);
        deleteMax(m -> eMagosH);
        delete aux;
    }
    destroyS(m -> eHechizos);
    destroyM(m -> eMagosM);
    destroyH(m -> eMagosH);
    delete m;
}


