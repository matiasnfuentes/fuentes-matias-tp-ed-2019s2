#include "set.h"
#include "../common/Common.h"

struct Node {
   Hechizo elem; // el elemento que este nodo almacena
   Node* next; // siguiente nodo de la cadena de punteros
};

struct SetSt {
   int size; // cantidad de elementos del conjunto
   Node* first; // puntero al primer elemento
};

/**
  Invariantes de representacion:
    - size es la cantidad de nodos
    - no hay nodos con hechizos repetidos
**/

/// Proposito: retorna un conjunto de hechizos vacio
/// Costo: O(1)

Set emptyS() {
   Set s = new SetSt;
   s -> size = 0;
   s -> first = NULL;
   return s;
}

/// Proposito: retorna la cantidad de hechizos
/// Costo: O(1)

int sizeS(Set s) {
   return s -> size;
}

/// Proposito: indica si el hechizo pertenece al conjunto
/// Costo: O(h), h = cantidad de hechizos

bool belongsS(Hechizo h, Set s) {
   Node* puntero = s -> first;
   while ( (puntero != NULL) && (!mismoHechizo(puntero -> elem,h)) ){
        puntero = puntero -> next;
   }
   return (puntero != NULL);
}

/// Proposito: agrega un hechizo al conjunto
/// Costo: O(h), h = cantidad de hechizos

void addS(Hechizo h, Set s) {
   Node* aux = s -> first;
   if(!belongsS(h,s)){
        Node* nn = new Node;
        s -> first = nn;
        nn -> next = aux;
        nn -> elem = h;
        s -> size = (s -> size) + 1;
   }
}

/// Proposito: borra un hechizo del conjunto (si no existe no hace nada)
/// Costo: O(h), h = cantidad de hechizos

void removeS(Hechizo h, Set s) {
   Node* puntero = s -> first;
   Node* aux = NULL;
   while((puntero != NULL) && (!mismoHechizo(puntero -> elem,h)) ){
            aux = puntero;
            puntero = puntero -> next;
    }
    if (puntero != NULL){
        if (aux == NULL){
            s -> first = aux;
        }
        else{
            aux -> next = puntero -> next;
        }
        s -> size = (s -> size) - 1;
        delete puntero;
    }
}

/// Proposito: borra toda la memoria consumida por el conjunto (pero no la de los hechizos)
/// Costo: O(n)

void destroyS(Set s) {
   Node* puntero = s -> first;
   Node* aux = NULL;
   while(puntero!= NULL){
        aux = puntero;
        puntero = puntero -> next;
        delete aux;
   }
}

/// Proposito: retorna un nuevo conjunto que es la union entre ambos (no modifica estos conjuntos)
/// Costo: O(h^2), h = cantidad de hechizos

Set unionS(Set s1, Set s2) {
   SetSt* ns = emptyS();
   Node* puntero = s1 -> first;
   while (puntero != NULL){
                addS(puntero -> elem, ns);
                puntero = puntero -> next;
        }
   puntero = s2 -> first;
   while (puntero != NULL){
                addS(puntero -> elem, ns);
                puntero = puntero -> next;
   }
   return ns;
}
